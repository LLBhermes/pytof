Python module to compute the exact resolution function for neutron reflectivity time-of-flight measurements.
****************************************************
D. Lairez, A. Chennevière, F. Ott

lairez@cea.fr

didier.lairez@polytechnique.edu
****************************************************
The structure of this module matches the paper:
https://arxiv.org/abs/1906.09577
****************************************************
Getting started: see and run test1.py
or try jupyter notebook
https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2FLLBhermes%2Fpytof%2Fsrc%2Fmaster/master?filepath=%2Fnotebook1.ipynb
****************************************************
Contents:

    pytof.py: main module; 

    notebook1.ipynb : jupyter notebook to try the module; 
    
    test1.py : examples of usage; 

    SiO2_MR.txt: reflectivity spectrum for test2.py.
