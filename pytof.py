# ******************************************************************************
# Python module to compute the resolution function for neutron reflectivity 
# time-of-flight measurements.
# D. Lairez, A. Chennevière, F. Ott
# lairez@cea.fr
# didier.lairez@polytechnique.edu
# ******************************************************************************
import numpy as np
import scipy.interpolate as interp
from scipy.optimize import least_squares
from scipy.signal import boxcar, convolve
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from lmfit import Parameters, minimize, fit_report
from scipy.special import erf
import time
# ******************************************************************************
# physical constants
hsm = 3956  # h/m (in A m/s)
grv = 9.81  # gravity (in m/s^2)
# ******************************************************************************


class tof():
    # **************************************************************************
    # Class for the calculation of resolution function
    # Usage to produce some figures:
    # >>> import pytof 
    # create instances with different parameters
    # >>> a=pytof.tof(chop_position=1, r1=0.5)
    # >>> b=pytof.tof(chop_position=2, r1=1)
    # >>> c=pytof.tof(chop_position=3, r1=2.3)
    # plot q-resolution
    # >>> a.figR()
    # >>> b.figR()
    # >>> c.figR()
    # plot wavelength-resolution
    # >>> a.figH()
    # update parameters and replot
    # >>> a.update(chop_phi2=-160)
    # >>> a.figH()
    # **************************************************************************
    def __init__(self, **var):
        # self.var is a dictionary to store variables (parameters of chopper etc)
        self.var = {}
        self.var0 = {}
        # Properties: results of calculations
        # 1D-arrays (one element per tof_channel)
        self.a = []     # channel phase (in °)
        self.i = []     # 1st disk the closure of which is limiting
        self.j = []     # 2nd disk the opening of which is limiting
        self.vi = []    # neutron velocity corresponding to disk i (in mm/°) 
        self.vj = []    # neutron velocity corresponding to disk j (in mm/°) 
        self.li = []    # wavelength corresponding to vi (in A)
        self.lj = []    # wavelength corresponding to vj (in A)
        self.l0 = []    # nominal wavelength: (li+lj)/2 (in A)
        self.q0 = []    # nominal transfer vector
        self.qm = []    # mean transfer vector
        self.qs = []    # standard deviation for transfer vector
        # 1D-arrays common for all tof-channels
        self.P = []     # angular distribution due to collimation
        self.alpha = [] # random variable for P
        # 2D-arrays (ni points per channel)
        self.lamb0 = [] # nominal wavelength
        self.lamb = []  # wavelength
        self.D0 = []    # wavelength rectangular distribution of transmission
        self.D = []     # wavelength distribution of transmission
        self.H = []     # wavelength distribution on the sample
        self.R = []     # resolution for transfer vector
        self.q = []     # transfer vector
        #
        self.var = self.variables(**var)
        self.var0 = self.var.copy()
        self.update()
    # **************************************************************************
    def variables(self, **var):
        """
        Build the var properties of the object
        
        """
        if bool(self.var0):
            s = self.var0
        else:
            s = {'ni': 100}               # sampling for distribution
        self.var.update({**s, **var})
        # default spectrometer
        if 'spectro' not in self.var:
            self.var['spectro'] = 'hermes'
        # Variabes that depend on the spectrometer
        if self.var['spectro'].lower() == 'hermes': # special case of HERMES@LLB
            s = {
            'aperture': 165,              # disk aperture (in °)
            'sample2detDist': 2020,       # sample to detector distance (in mm)
            'chop3_detector_dist': 2375,  # disk3 to detector dist. (in mm)
            'd_chop2_chop3': 1999,        # disk2 to disk3 dist. (in mm)
            'pos': [-102, -351, -1004],   # 3 possible distances (in mm) between disk1 and disk2
            'delta': 0,                   # tilt angle of disk 2 (in °)
            'eta': 0,                     # tilt angle of disk 3 (in °)
            'acq_origin_ofphase': 299.25, # phase angle between the trigger of time-recording and the beam opening
            'r' : 300,                    # radius of disk-chopper (in mm)
            'chop2_entryCollim': 100      # distance between disk 2 and the entry of the collimator (in mm)
           }
            self.var.update({**s, **self.var})
        elif self.var['spectro'] == 'other':
            pass               # complete here for another spectrometer       
        # Variables that depend on the measurement configuration
        s = {
            'angle': 1,         # nominal angle (in °)
            'acq_start': 30,    # phase of counting start (in °)
            'acq_aperture': 300,# aperture angle of acquisition window (in °)
            'sam': None,        # sample time (µs)
            'delay': None,      # time delay of acquisition (µs)
            'nb_channels': 256, # number of TOF channels
            'speed': 1800,      # velocity of the chopper in rpm
            'chop_position': 2, # index of disk1-position (1, 2 or 3)
            'chop_phi2': -165,  # phase of disk2 (in °)
            'chop_phi3': -165,  # phase of disk3 (in °)
            'r1': 1,            # half-width of the 1st slit of collimator (in mm)
            'r2': 0.5,          # half-width of the 2nd slit (in mm)
            'dc': 1800,         # distance between slits (in mm)
            'd1': 1000          # distance between middle of collimator and sample (in mm)            
            }
        self.var.update({**s, **self.var})
        # some additional variables computed from others
        self.var['theta0'] = self.var['angle'] * np.pi / 180             # nominal angle (in rd)
        self.var['w'] = self.var['speed'] * 360.0 / 60000                # chopper pulsation (in °/ms)
        self.var['hsmw'] = hsm / self.var['w']                           # convertion factor velocity -> wavelength
        x1 = self.var['pos'][self.var['chop_position']-1]                # position (in mm) of disk1
        x3 = self.var['d_chop2_chop3']                                   # position (in mm) of disk3
        x4 = self.var['chop3_detector_dist'] + self.var['d_chop2_chop3'] # position of detector
        self.var['x'] = np.array([x1, 0, x3, x4])-x1/2                   # physical axis for distances
        # acquisition phases
        if self.var['sam'] != None and self.var['delay'] != None:
            # if sam and delay are provided they override acq_start and acq_aperture
            self.var['acq_start'] = (0.5 * self.var['sam'] + self.var['delay']) * self.var['w'] / 1000 - self.var['acq_origin_ofphase']
            self.var['acq_aperture'] = (self.var['nb_channels'] * self.var['sam']) * self.var['w'] / 1000
        # phases for opening and closure
        self.var['phi_open'] = np.array([
                                        0,
                                        -self.var['chop_phi2']+self.var['delta'],
                                        -self.var['chop_phi3']+self.var['eta'],
                                        self.var['aperture']+self.var['acq_start']
                                        ])
        self.var['phi_close'] = self.var['phi_open'] + self.var['aperture']
        self.var['phi_close'][3] = self.var['phi_open'][3] + self.var['acq_aperture']
        # change of origin
        epsilon = (self.var['phi_close'][0]+self.var['phi_open'][1]) / 2
        self.var['phi_open'] = self.var['phi_open'] - epsilon
        self.var['phi_close'] = self.var['phi_close'] - epsilon
        h = (self.var['x'][0] + self.var['x'][1]) / 2
        self.var['x'] = self.var['x'] - h
        #
        self.var['xin'] = self.var['chop2_entryCollim'] + self.var['x'][1]      # collimator entry
        self.var['xc'] = self.var['xin'] + self.var['dc']/2                     # middle of collimator
        self.var['alphar'] = (self.var['r1'] - self.var['r2']) / self.var['dc'] # beam divergence      
        self.var['da'] = self.var['acq_aperture'] / self.var['nb_channels'] / 2 # channel half-width (in °)
        # final filter of unnecessary variables
        if bool(self.var0):
            self.var = dinject(self.var, self.var0)
            self.var0 = self.var.copy()
        
        return self.var

    def reset(self):
        # reset self.var to default values
        self.var = self.var0

    def update(self, **var):
        # Main function that calls all calculations
        s = self.variables(**var)
        # compute phase for acquisition
        self.a = np.linspace(s['phi_open'][3], s['phi_close'][3], s['nb_channels'])        
        self.fIJ()
        self.fD()
        self.fH()
        self.fP()
        self.fR()

    def disp(self, **var):
        s = self.variables(**var)
        for si in s:
            print(si, ':', s[si])
            
    # **************************************************************************
    # Calculations of the different distribution functions involved
    # **************************************************************************
    def fHB(self, lamb):
        # Wavelength distribution of the incident beam.
        # This is an ad-hoc function that aims to parametrize the distribution.
        # Input:
        #   lamb: wavelength (in A)
        # Output:
        #   HB: probability density
        # **********************************************************************
        def lnsumpeak(x, a, mu, sigma):
            # Sum of log-normale: a, mu,
            # of same length equal to the number of peaks
            #   a: amplitudes**var1000
            #   mu: means
            #   sigma: standard deviations
            def lnnormpdf(x, mu, sigma):
                # Kind of log normal distribution
                def normpdf(x, mu, sigma):
                    return np.exp(-0.5 * ((x - mu) / sigma)**2) / (np.sqrt(2 * np.pi) * sigma)
                y = normpdf(np.log10(x), np.log10(mu), sigma)
                y0 = normpdf(np.log10(mu), np.log10(mu), sigma)
                return y / y0
            y=np.zeros(np.shape(x))
            for i in range(len(mu)):
                y = y + a[i] * lnnormpdf(x, mu[i], sigma[i])
            return y

        if self.var['spectro'] == 'hermes':
            p=np.array([[0.1118, 2.1820, 0.1061], [0.1953, 3.3824, 0.1963]])
            HB=lnsumpeak(lamb, p[:,0], p[:,1], p[:,2])
        else:
            HB=np.ones(np.shape(lamb))
        return HB

    def fIJ(self):
        # Compute the object properties:
        #   self.i: 1st disk the closure of which is limiting for transmission
        #   self.j: 2nd disk the opening of which is limiting for transmission
        #   self.vi: neutron velocity corresponding to disk i (in mm/°) 
        #   self.vj: neutron velocity corresponding to disk j (in mm/°) 
        #   self.li, self.lj: corresponding wavelength
        #   self.l0: nominal wavelength
        #   self.q0: nominal value of transfer vector
        # All these properties are 1D-array: 1 value per tof-channel
        #   self.lamb0: copy of self.l0 in a 2D-array to facilitate surface plots
        # **********************************************************************
        def tslope(x2, x1, fi2, fi1):
        # Slope of the trajectory (in mm/°) in a diagram position vs. phase.
        # x1, x2: position 1 and 2 (in mm)
        # fi1, fi2: corresponding phase (in °)
            denom = fi2 - fi1
            r = np.full(np.shape(denom), np.Inf)
            i = denom>0
            r[i] = (x2 - x1) / denom[i]
            return r
        # **********************************************************************
        s = self.var
        # overall boundary of acquisition-window:
        # 1) fastest neutrons
        vmax1 = tslope(s['x'][1], s['x'][0], s['phi_open'][1], s['phi_close'][0])
        vmax2 = tslope(s['x'][2], s['x'][0], s['phi_open'][2], s['phi_close'][0])
        vmax = np.amin([vmax1, vmax2], axis=0)
        # 2) slowest neutrons
        vmin = tslope(s['x'][2], s['x'][1], s['phi_close'][2], s['phi_open'][1])
        # Boundaries per tof-hannel
        # 1) fastest neutrons
        v1a = tslope(s['x'][3], s['x'][0], self.a, s['phi_close'][0])
        v1b = tslope(s['x'][3], s['x'][1], self.a, s['phi_close'][1])
        v1c = tslope(s['x'][3], s['x'][2], self.a, s['phi_close'][2])
        arv1 = [v1a, v1b, v1c]
        self.vi = np.amin(arv1, axis=0)
        self.i = np.argmin(arv1, axis=0)
        # 2) slowest neutrons
        v2a = tslope(s['x'][3], s['x'][0], self.a, s['phi_open'][0])
        v2b = tslope(s['x'][3], s['x'][1], self.a, s['phi_open'][1])
        v2c = tslope(s['x'][3], s['x'][2], self.a, s['phi_open'][2])
        arv2 = [v2a, v2b, v2c]
        self.vj = np.amax(arv2, axis=0)
        self.j = np.argmax(arv2, axis=0)
        # remove wrong values
        self.vi[self.vi>vmax] = np.NaN
        self.vj[self.vj<vmin] = np.NaN
        # wavelength boundaries for each tof channel
        self.li = s['hsmw'] / self.vi
        self.lj = s['hsmw'] / self.vj
        # median wavelength
        self.l0 = (self.li + self.lj) / 2 
        # nominal transfer vector value
        self.q0 = 4 * np.pi * np.sin(s['theta0']) / self.l0
        self.lamb0 = np.repeat(self.l0[np.newaxis, :], self.var['ni'], 0)
            
    def fD(self):
        # Compute the object properties (2D-arrays of ni points per channel):
        #   self.D0: wavelength raw distribution of transmission (boxcar between li and lj)
        #   self.D: wavelength distribution of chopper transmission (array of rows, 1 row per TOF channel)
        #   self.lamb: wavelength corresponding to each D value
        def one_channel(k):
            Li=s['x'][3]-s['x'][self.i[k]]
            Lj=s['x'][3]-s['x'][self.j[k]]
            # half-width of boxcars due to crossing time
            # 1) closure: i
            rai = s['r1'] + s['alphar'] * (s['xin'] - s['x'][self.i[k]]) # beam size on disk i
            phicross_i = rai / s['r'] / np.pi * 180 # in degree
            wci=s['hsmw'] * phicross_i / Li
            # 2) closure: j
            raj = s['r1'] + s['alphar'] * (s['xin'] - s['x'][self.j[k]])
            phicross_j = raj / s['r'] / np.pi * 180 # in degree
            wcj = s['hsmw'] * phicross_j / Lj
            # half-width of boxcars due to channel width
            # channel width expressed in phase-angle
            wdi = s['hsmw'] * s['da'] / Li
            wdj = s['hsmw'] * s['da'] / Lj
            # wavelength range
            sc = (wci + wdi)
            lamb = np.linspace(self.li[k]-sc, self.lj[k]+sc, s['ni'])
            # Oversampling of wavelength range for convolution
            lc = np.linspace(self.li[k], self.lj[k], 2*s['ni'])
            # This is to ensure that the edges of unit-step functions coincide with a wavelength sample.
            # Otherwise sampling distorts resolution curves from one channel to the other
            Dl = lc[2]-lc[1]          
            nbDl = int(np.round(sc/Dl)) + 4      
            a = np.linspace(self.li[k]-nbDl*Dl, self.li[k]-Dl, nbDl-1)
            b = np.linspace(self.lj[k]+Dl, self.lj[k]+nbDl*Dl, nbDl-1)
            l = np.concatenate([a, lc, b])
            # unit step functions for closure 
            f1 = np.zeros(l.shape)
            f1[l==self.li[k]] = 0.5
            f1[l>=self.li[k]] = 1         
            # and aperture
            f2 = np.zeros(l.shape)
            f2[l==self.lj[k]] = 0.5
            f2[l<=self.lj[k]] = 1
            # boxcar
            D0 = f1 * f2
            # convolution
            g1 = self.convbox(l, f1, wci)
            g2 = self.convbox(l, f2, wcj)
            h1 = self.convbox(l, g1, wdi)
            h2 = self.convbox(l, g2, wdj)
            D = h1 * h2
            # downsampling
            D0 = pchip(l, D0, lamb)
            D = pchip(l, D, lamb)
            # normalization
            D0 = D0 / sum(D0)
            D = D / sum(D)
            return D0.squeeze(), D.squeeze(), lamb.squeeze()
            
        s = self.var
        # loop over the 2nd dim. of a 2D-array
        r = np.array([one_channel(k) for k in np.arange(self.a.size)])
        self.D0 = r[:, 0].T
        self.D = r[:, 1].T
        self.lamb = r[:, 2].T
            
    def fH(self):
        # Compute the object properties (2D-arrays of ni points per channel):
        #   self.H: wavelength distribution on sample
        self.H = self.D * self.fHB(self.lamb)
        self.H = self.H /  np.sum(self.H, axis=0)

    def fP(self):
        # Compute the object properties (2D-arrays of ni points per channel):
        #   self.P: probability density of angles alpha
        #   self.alpha: angle (in rd)
        # Note: valid for small angle i.e. tan(alpha) = alpha
        s = self.var
        ae = (s['r1'] + s['r2']) / s['dc'] 
        self.alpha = np.linspace(-ae, ae, s['ni'])
        self.P = np.ones(self.alpha.shape)
        ni = np.abs(self.alpha) > ((s['r1'] - s['r2']) / s['dc'])
        p = -1/(2 * s['r2'] / s['dc'])
        self.P[ni] = 1 + (np.abs(self.alpha[ni]) - (s['r1'] - s['r2']) / s['dc']) * p
        self.P = self.P / np.sum(self.P)

    def fR(self):
        # Compute the object properties (2D-arrays of ni points per channel):
        #   self.R: resolution for q (prob; densities for q)
        #   self.q: q-values for R
        s = self.var
        c = np.sqrt(grv * s['d1'] * 1e-3) / hsm  # constant for the gravity-deviation
        pi4 = 4 * np.pi
        Px = np.repeat(self.alpha[:, np.newaxis], self.H.shape[self.H.ndim-1], axis=1)
        P = np.repeat(self.P[:, np.newaxis], self.H.shape[self.H.ndim-1], axis=1)
        #
        f = lambda l, a: pi4 * np.sin(s['theta0'] + a + (c*l)**2) / l
        g = lambda l, q: np.arcsin(l * q / pi4) - s['theta0'] - (c*l)**2
        gp= lambda l, q: l / np.sqrt(pi4 - (l*q)**2)
        #
        [self.q, self.R] = self.conv(self.lamb, self.H, Px, P, f, g, gp)
        self.q = np.abs(self.q)
        [self.qm, self.qs] = charac_distrib(self.q, self.R)


    # **************************************************************************
    # Functions for convolution
    # **************************************************************************
    def convbox(self, a, Pa, da):
        # Compute the convolution of the distribution Pa
        # by a boxcar of half-width da
        # Input: 
        #   a: variable (1D-array)
        #   Pa: distribution function for a
        #   da: half-width of the box (ie uncertainty on a)
        # Output:
        #   Px: convolution of Pa
            n = 2 * np.round(da / (a[1]-a[0])) + 1
            if n > 1:
                w = boxcar(n.astype(int))
                Px = np.expand_dims(convolve(Pa.squeeze(), w, mode='same'),1)
                return Px
            else:
                return Pa
        
    def conv(self, a, Pa, b, Pb, f, g, gp):
        # Generalized convolution for random variables.
        # If A is a random variable of density Pa with support a,
        # and B a random variable of density Pb with support b
        # The function calculate the density (Px) and its support (x)
        # of the random variable X=f(A,B).
        # Input:
        #   a, Pa, b, Pb: 1D-arrays (e.g. 1D-array per tof-channel) or array of 1D-arrays
        #   f: handle of the 'transfert' function f: (A,B)-->X
        #   g: handle of the function g:(A,X)-->B
        #   gp:handle of the derivative of g with respect to X
        # Output:
        #   x: support for X
        #   Px: density
        # Usage:
        # example 1): X=A+B (simple convolution)
        #   [x,Px]=conv(a, Pa, b, Pb, lambda a, b: a+b, lambda a, x: x-a, lambda a, x: np.ones(a.shape))
        # example 2): X=B/A
        #   [x,Px]=conv(a, Pa, b, Pb, lambda a, b: b/a, lambda a, x: a*x, lambda a, x: a)
        if a.ndim==1:
            if ~np.sum(np.isnan(a)) and ~np.sum(np.isnan(b)):
                x = self.support(a, b, f) # 1D array of length ni
                [va, vx] = np.meshgrid(a, x)
                vb = g(va, vx)       # values for b, matrix ni x length(a) (ni rows of same length than a)
                pvb = pchip(b, Pb, vb, np.amin(b), np.amax(b)) # corresponding probability density
                dvb = gp(va, vx)     # derivative of b

                Px = np.dot(Pa, (dvb*pvb).T)  # convolution
                Px = Px / np.sum(Px)          # normalization
            else:
                x = np.full(a.shape, np.NaN)
                Px= np.full(a.shape, np.NaN)
            return x, Px
        else:
           # loop over the 2nd dim. of a 2D-array
           r = np.array([ self.conv(a[:, i], Pa[:, i], b[:, i], Pb[:, i], f, g, gp)
                          for i in np.arange(a.shape[a.ndim-1])])
           return r[:, 0, :].T, r[:, 1, :].T

    def support(self, a, b, f):
        # Compute the compact support of random variable f(A,B)
        # Input:
        #     a: compact support of random variable A (1D array)
        #     b: compact support of random variable B (1D array)
        #     f: handle of function f(A,B)
        # Output:
        #     x: compact support of random variable X=f(A,B) (1D-array of length vap['ni'])
        va, vb = np.meshgrid(a, b)
        vx = f(va, vb)
        x1 = np.amin(vx)
        x2 = np.amax(vx)
        x = np.linspace(x1, x2, self.var['ni']).squeeze()
        return x

    # **************************************************************************
    # Some figures to visualize distributions
    # **************************************************************************
    def figD0(self):
        # Plot the chopper rectangular transmission
        plt.figure()
        ax = plt.axes(projection='3d', title='Wavelength distribution')
        x, y, z = formatting(self.lamb0, self.lamb/self.lamb0, self.D0)
        ax.plot_surface(x, y, z, cmap=cm.get_cmap('coolwarm'), linewidth=0, antialiased=False)
        ax.view_init(60, -30)
        ax.set_xlabel(r'TOF channel $\lambda_0$ (Å)')
        ax.set_ylabel(r'$\lambda/\lambda_0$ (Å)')
        ax.set_zlabel(r'$D(\lambda)$')
        plt.show(block=False)

    def figD(self, **var):
        # Plot the chopper actual transmission for all channels
        plt.figure()
        ax = plt.axes(projection='3d', title='Wavelength distribution')
        x, y, z = formatting(self.lamb0, self.lamb/self.lamb0, self.D)
        ax.plot_surface(x, y, z, cmap=cm.get_cmap('coolwarm'), linewidth=0, antialiased=False)
        ax.view_init(60, -30)
        ax.set_xlabel(r'TOF channel $\lambda_0$ (Å)')
        ax.set_ylabel(r'$\lambda/\lambda_0$ (Å)')
        ax.set_zlabel(r'$D(\lambda)$')
        plt.show(block=False)

    def figH(self, **var):
        # Plot the wavelength distribution on the sample for all channels
        plt.figure()
        ax = plt.axes(projection='3d', title='Wavelength distribution')
        x, y, z = formatting(self.lamb0, self.lamb/self.lamb0, self.H)
        ax.plot_surface(x, y, z, cmap=cm.get_cmap('coolwarm'), linewidth=0, antialiased=False)
        ax.view_init(60, -30)
        ax.set_xlabel(r'TOF channel $\lambda_0$ (Å)')
        ax.set_ylabel(r'$\lambda/\lambda_0$ (Å)')
        ax.set_zlabel(r'$H(\lambda)$')
        plt.show(block=False)

    def figP(self, **var):
        # Plot the angle distribution due to collimation for one given channel
        plt.figure()
        x = self.alpha * 180 / np.pi
        y = self.P
        plt.plot(x, y)
        plt.xlabel(r'$\alpha$ (°)')
        plt.ylabel(r'$P(\alpha)$')
        plt.title(f'Angle distribution due to collimation')
        plt.show(block=False)

    def figR(self, **var):
        # Plot of the total resolution function of transfert vector q.
        plt.figure()
        ax = plt.axes(projection='3d', title='Resolution function')
        x, y, z = formatting(self.lamb0, self.q/self.q0, self.R)
        ax.plot_surface(x, y, z, cmap=cm.get_cmap('coolwarm'), linewidth=0, antialiased=False)
        ax.view_init(60, -30)
        ax.set_xlabel(r'TOF channel $\lambda_0$ (Å)')
        ax.set_ylabel(r'$q/q_0$')
        ax.set_zlabel(r'$R(q)$')
        plt.show(block=False)

    def figDeltaQ(self, **var):
        # plot relative width versus average q
        plt.figure()
        x = self.q0
        y = self.qs / self.qm
        plt.plot(x, y)
        plt.xlabel(r'$q_0$ (Å$^{-1}$)')
        plt.ylabel(r'$\sigma_q/\bar q$')
        plt.title(f'Relative width of transfer vector resolution')
        plt.show(block=False)

    def figDeltaL(self, **var):
        # plot relative width versus average q
        plt.figure()
        x = self.l0
        [mx, sx] = charac_distrib(self.lamb, self.H)
        y = sx / mx
        plt.plot(x, y)
        plt.xlabel(r'$\lambda_0$ (Å)')
        plt.ylabel(r'$\sigma_\lambda/\bar \lambda$')
        plt.title(f'Relative width of wavelength resolution')
        plt.show(block=False)

class data():
    # **************************************************************************
    # Class for data handling.
    # This class is just a getting started example for data read, plot, 
    # compute full resolution and fit.
    # **************************************************************************
    def __init__(self, file=None, x=np.array([]), y=np.array([]), dy=np.array([]), xlabel='x', ylabel='y', meta={}):
        self.x = x
        self.y = y
        self.dy = dy
        self.meta = meta # meta data dictionary
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.sfit = None
        if file is not None:
            self.readtxt(file)
        self.res = tof(**self.meta) # compute resolution from meta-data
    
    def plotraw(self, xscale='linear', yscale='log'):
        # Plot y vs. x
        if self.y.size>0:
            plt.figure()
            plt.errorbar(self.x, self.y, self.dy, marker='.', linestyle=' ', label='data')
            plt.xlabel(self.xlabel)
            plt.ylabel(self.ylabel)
            plt.xscale(xscale)
            plt.yscale(yscale)
            plt.show(block=False)
            
    def plot(self, xscale='linear', yscale='log'):
        # Plot y vs. x
        if self.y.size>0:
            plt.figure()
            plt.errorbar(1/self.res.qm, self.y, self.dy, marker='.', linestyle=' ', label='data')
            if self.sfit is not None:
                plt.plot(1/self.res.qm, self.sfit.yfit, label='best fit')
                plt.gca().legend()
            plt.xlabel('$1/q$ (Å)')
            plt.ylabel(self.ylabel)
            plt.xscale(xscale)
            plt.yscale(yscale)
            plt.show(block=False)

    def readtxt(self, fn):
        # Read  and parse a text file made of:
        # 1) one header 'key: value'
        # 2) one blank line
        # 3) three columns of values: x, y, dy
        self.meta = {}
        d = []
        header = True
        with open(fn) as f:
            for line in f:
                if line=='\n':
                    header = False
                else:
                    if header:
                        k, v = line.strip().split(':')
                        self.meta[k] = str2num(v.strip())
                    else:
                        d.append(line.rstrip().split())
            d = (np.asarray(d)).astype(float)
            self.x = d[:, 0]
            self.y = d[:, 1]
            self.dy = d[:, 2]
            self.file = fn
            self.xlabel = 'index of tof-channel'
            self.ylabel = 'reflectivity'

    def fit(self, sld,d,sigma):
        # data fitting
        #   p0: initial parameters
        p0=np.hstack(sld,d,sigma)
        def resid(p):
            # objective function for weighted least-square
            nb,e,s=np.split(p,3)
            return (reflParrat(self.res.q,nb,e,s,R=self.res.R) - self.y)/self.dy
        
        self.sfit  = least_squares(resid, p0)
        self.sfit.p = self.sfit.x
        sldFit,dFit,sigmaFit=np.split(self.sfit.p,3)
        self.sfit.yfit = reflParrat(self.res.q,sldFit,dFit,sigmaFit,R=self.res.R)
        self.plot()


# ******************************************************************************
# Miscellaneous functions
# ******************************************************************************
def asarray(a):
    # Generalized numpy.asarray
    if np.isscalar(a):
        return np.asarray([a])
    else:
        return np.asarray(a)

def linspace(a, b, n):
    # Generalized numpy.linspace for a and b as 1D-arrays
    a = asarray(a)
    b = asarray(b)
    d = (b-a)/(n-1)
    m = np.size(a)
    c = np.arange(n)
    c = np.repeat(c[:, np.newaxis], m, 1)
    c = a + c * d
    return c

def pchip(fx, fy, v, mi=-np.inf, ma=np.inf):
    # Bounded interpolation for distribution (0 outside the intervalle [mi, ma])
    if sum(np.isnan(fx))>0 or sum(np.isnan(fx))>0:
        p = np.zeros(v.shape)
    else:
        interpolation = interp.PchipInterpolator(fx, fy)
        p = interpolation(v)
        v[np.isnan(v)] = np.inf
        p[np.logical_or(v<mi, v>ma)] = 0
    return p

def dinject(d, dtarget):
    # d and dtarget are dictionaries.
    # Inject d values which correspond to valid keys of dtarget
    r = dtarget.copy()
    k = r.keys()
    for ki in d:
        if ki in k:
            r[ki] = d[ki]
    return r

def str2num(var):
    # Convert string to numerical value (if possible)
    try:
        if int(var) == float(var):
            return int(var)
    except:
        try:
            return float(var)
        except:
            return var

# ******************************************************************************
# Miscellaneous for 2D-array formating
# ******************************************************************************
def downsampling(x, y, z, ratio=10):
    # reduce the number of tof channels (2nd dimension of 2D-arrays)
    i = np.arange(0, x.shape[1], ratio)
    return x[:, i], y[:, i], z[:, i]

def trailing_zero(x, y, z):
    # Adds zeros outside the support (along the x-axis) of distribution for
    # better surface plot.
    if z.ndim > 1:
        a = np.zeros([1, z.shape[1]])
        nz = np.append(a, np.append(z, a, axis=0), axis=0)
        #
        dx = x[1, :] - x[0, :]
        a1 = (x[0, :]-dx)[np.newaxis, :]
        a2 = (x[x.shape[0]-1,:]+dx)[np.newaxis, :]
        nx = np.append(a1, np.append(x, a2, axis=0), axis=0)
        #
        dy = y[1, :] - y[0, :]
        a1 = (y[0, :]-dy)[np.newaxis, :]
        a2 = (y[y.shape[0]-1,:]+dy)[np.newaxis, :]
        ny = np.append(a1, np.append(y, a2, axis=0), axis=0)
        return nx, ny, nz

def formatting(x, y, z, down_sampling_ratio=1):
    z = normalize(y, z)
    x, y, z = downsampling(x, y, z, ratio=down_sampling_ratio)
    x, y, z = trailing_zero(x, y, z)
    return x, y, z

def normalize(x, Px):
    # normalization of Px in order the integral over x is 1
    dx = np.diff(x,axis=0)
    dx = np.row_stack([dx[0,:], dx])
    p = Px / np.sum(Px, axis=0) / dx
    return p

def charac_distrib(x, Px):
    # return the mean (m) standard deviation (s) of the distribution Px of x
    m = np.sum(Px*x, axis=0)
    s = np.sqrt(np.sum(Px*x**2, axis=0) - m**2)
    return m, s

# *****************************************************************************
# Model functions for reflectivity
# *****************************************************************************
def Fresnel(p, q, R=None):
    # Reflectivity of a single interface
    # p[0]: R(q)=0
    # p[1]: critical q value
    # p[2]: roughness
    # q: scattering vector
    # R: resolution function for q
    # Two possibilities:
    #   1) R is not provided and q is a 1D-array (or a scalar).
    #      Then the theoretical function is calculated.
    #   2) R is provided and has the same dimension than q.
    #      Typically, q and R are 2D-arrays.
    #      Then, the convolution by R of the theoretical function is calculated.
    y = np.ones(q.shape)
    nani = np.isnan(q)
    vq = q
    vq[nani] = -np.inf   # because comparison with NaN is not possible
    oki = vq > p[1]
    #
    qs = np.sqrt(q[oki]**2 - p[1]**2)
    y[oki] = ((q[oki] - qs) / (q[oki] + qs))**2
    y[oki] = y[oki] * np.exp(-p[2]**2 * q[oki] * qs)
    y = p[0] * y
    y[nani] = np.NaN
    # do convolution if R is provided
    if R is not None: y = np.sum(y * R, axis=0)
    return y

# *****************************************************************************
# Model functions for reflectivity of slabs (Parrat Algorythm)
# *****************************************************************************
def reflParrat(q,sld,d,sigma,R=None):
    sld=sld*1e-6
    d[0]=0
    d[-1]=0
    s2=sigma**2
#    d=d[:-1]
#    sigma=sigma[:-1]
    qj=np.sqrt(q**2/4-4*np.pi*(sld[:,np.newaxis,np.newaxis]-sld[0]),dtype='complex128')
    r=(qj[1:,:,:]-qj[:-1,:,:])/(qj[1:,:,:]+qj[:-1,:,:])
    y=r[-1,:,:]*np.exp(-2*qj[-2,:,:]*qj[-1,:,:]*s2[-2])
    N=len(sld)
    k=N-2
    while k>0:
        a=np.exp(-2*qj[k-1,:,:]*qj[k,:,:]*s2[k-1])
        y=(a*r[k-1,:,:]+y*np.exp(-2j*d[k]*qj[k,:,:]))/(1+r[k-1,:,:]*a*y*np.exp(-2j*d[k]*qj[k,:,:]))
        k-=1
    y=np.abs(y)**2
    if R is not None: 
        y = np.sum(y * R, axis=0)
    return y
# *****************************************************************************
# Classes for stacked layers 
# *****************************************************************************
class Layer(object):
    def __init__(self,name,sld=0,d=0,sigma=0,vary=[False,False,False],
                 minValue=[-2,0,0],maxValue=[100,1e6,1e6]):
        self.name=name
        self.parameters=Parameters()
        self.parameters.add(name+'_sld', value=sld,
                            vary=vary[0],
                            min=minValue[0], max=maxValue[0])
        self.parameters.add(name+'_d', value=d,
                            vary=vary[1],
                            min=minValue[1], max=maxValue[1])
        self.parameters.add(name+'_sigma', value=sigma,
                            vary=vary[2],
                            min=minValue[2], max=maxValue[2])

        self.set_parameters(self.parameters)
    def get_profile(self,params):
        self.set_parameters(params)
        return self.sld, self.d, self.sigma
    def set_parameters(self,parameters):
        self.parameters.update(parameters)
        self.sld=self.parameters[self.name+'_sld'].value
        self.d=self.parameters[self.name+'_d'].value
        self.sigma=self.parameters[self.name+'_sigma'].value
    def show(self):
        self.parameters.pretty_print(columns=['value','vary'
                                              ,'min','max','stderr'])
        
class Stack(object):
    def __init__(self,layerList):
        self.layerList=layerList
        self.parameters=Parameters()
        for layer in layerList:
            self.parameters.update(layer.parameters)
        self.set_parameters(self.parameters)

    def set_parameters(self,parameters):
        self.parameters.update(parameters)
        sld=np.array([])
        d=np.array([])
        sigma=np.array([])
        for layer in self.layerList:
            keys=layer.parameters.keys()
            newParams=Parameters()
            for key in keys:
                newParams.add(self.parameters[key])
            sldLayer, dLayer, sigmaLayer=layer.get_profile(newParams)
            sld=np.append(sld,sldLayer)
            d=np.append(d,dLayer)
            sigma=np.append(sigma,sigmaLayer)
        self.sld=sld
        self.d=d
        self.sigma=sigma

    def get_profile(self,params):
        self.set_parameters(params)
        return self.sld, self.d, self.sigma
    
    def plot_profile(self):
        sld, d, sigma=self.get_profile(self.parameters)
        plt.figure('SLD profile')
        plt.cla()
        ax=plt.gca()
        ax.set_xlabel('$z\,[\AA]$')
        ax.set_ylabel('SLD [$10^{-6}\,\,\AA^{-2}$]')
        z,sld=sldProfile(sld,d,sigma)
        plt.plot(z,sld)
    def show(self):
        self.parameters.pretty_print(colwidth=5,
                                     columns=['value','vary'
                                              ,'min','max','stderr'])
        
def sldProfile(Nb, d, sigma, zStep=0.5):
    #compute sld profile with z=0 being the 1st interface
    zint = np.cumsum(abs(d[:-1]))
    z=np.linspace(-20-4*sigma[0],zint[-1]+2*sigma[-2]+20,int((zint[-1]+40+sigma[-2]+sigma[0])/zStep))
    rho=np.zeros_like(z)
    rho=0.5*(Nb[1:]-Nb[:-1])*erf((z[:,np.newaxis]-zint[:])/(2**0.5*sigma[:-1]))+0.5*(Nb[1:]-Nb[:-1])
    rho=np.sum(rho,1)+Nb[0]
    return z, rho

class RefModel(object):
    def __init__(self,data=None,stack=Stack([])):
        self.set_data(data)
        self.res = tof(**self.meta)
        self.stack=stack
#        model's parameters are composed of layer parameters
        self.sim()

    def sim(self,params=None,x=None):
        if x is None:
            x=self.res.q
        else:
            x=x
        if params is None:
            params=self.stack.parameters
        sld,d,sigma=self.stack.get_profile(params)
        self.ySim=reflParrat(x,sld,d,sigma,R=self.res.R)
            
            
            
    def fit(self,method='least_squares',residual='chi2'):
        def resi(params,x,y,dy):
            self.sim(x=x,params=params)
            return (self.ySim-y)/dy
        def logResi(params,x,y,dy):
            self.sim(x=x,params=params)
            return (np.log10(self.ySim)-np.log10(y))/dy

        if residual=='chi2':
            fun=resi
        elif residual=='log':
            fun=logResi
        else:
            print('Wrong figure of merit.\nPlease choose between chi2 or log')
            return

        t0=time.time()
        self.results=minimize(fun,self.stack.parameters,method=method,
                              args=(self.res.q,self.y,self.dy),nan_policy='omit',
                              reduce_fcn='None')
        t1=time.time()
        print('fit lasted %5e s'%(t1-t0))
        self.stack.parameters=self.results.params
        self.sim()
        print(fit_report(self.results))
        
#

    def plot(self,mode='Q'):
        plt.figure('spectra')
        plt.cla()
        if mode=='Q':
            x=self.res.qm
            y=self.y
            ySim=self.ySim
            dy=self.dy
            ylabel='$R$'
        elif mode=='Fresnel':
            Rf=reflParrat(self.x,np.array([0,6.36]),np.array([0,0]),
                     np.array([0,0]),R=self.res.R)
            y=self.y/(Rf)
            dy=self.dy/Rf
            ySim=self.ySim/Rf
            ylabel='$R/R_F$'
        elif mode=='RQ4':
            x=self.res.qm
            y=self.y*self.x**4
            dy=self.dy*self.x**4
            ySim=self.ySim*self.x**4
            ylabel='$RQ^4$ [$\AA^{-4}$]'
        plt.errorbar(x,y,yerr=dy,fmt='o',mfc='None')
        ax=plt.gca()
        ax.set_xscale('linear')
        ax.set_yscale('log')
        ax.set_xlabel('Q [$\AA^{-1}]$')
        ax.set_ylabel(ylabel)
        plt.semilogy(self.res.qm,ySim)
        plt.show()

    def set_data(self,file):
         # Read  and parse a text file made of:
        # 1) one header 'key: value'
        # 2) one blank line
        # 3) three columns of values: x, y, dy
        self.meta = {}
        d = []
        header = True
        with open(file) as f:
            for line in f:
                if line=='\n':
                    header = False
                else:
                    if header:
                        k, v = line.strip().split(':')
                        self.meta[k] = str2num(v.strip())
                    else:
                        d.append(line.rstrip().split())
            d = (np.asarray(d)).astype(float)
            self.x = d[:, 0]
            self.y = d[:, 1]
            self.dy = d[:, 2]
            self.file = file
            self.xlabel = 'index of tof-channel'
            self.ylabel = 'reflectivity'

    def show(self):
        self.stack.parameters.pretty_print(colwidth=5,
                                     columns=['value','vary'
                                              ,'min','max','stderr'])
    def saveFitAsTxt(self,filename):
        report=fit_report(self.results)
        lines=report.splitlines()
        with open(filename+'_fit.txt','w') as fid:
            for line in lines:
                fid.write('#'+line+'\n')
            fid.write('# %s \t %s \n'%('Q simu [Ang^-1]','I sim [cm^-1]'))
            mat=np.array([self.res.qm,self.ySim])
            mat=mat.transpose()
            for i in range(mat.shape[0]):
                fid.write('%f \t %f \n'%tuple(mat[i,:]))
    
if __name__ == '__main__':
    air=Layer('air',sld=0,d=0,sigma=20)
    quartz=Layer('quartz',sld=3.8,d=0,sigma=0)
    stack=Stack([air,quartz])
    model=RefModel(data='SiO2_MR.txt',stack=stack)
    model.stack.parameters['quartz_sld'].vary=True
    model.stack.parameters['air_sigma'].vary=True
#    model.stack.parameters['air_sigma'].value=20
    model.fit()
    model.stack.parameters.pretty_print()
    model.plot()
