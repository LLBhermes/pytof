# Import the library:
import pytof 
# Create an instance of the object "tof":
a=pytof.tof(chop_position=2, r1=1)
# This instance automatically computes the resolution 
# with default values for variables that can be modified 
# with the argument list.
# Display the variables and their current values:
a.disp()
# Modify one (or more) default value
a.update(chop_phi2=-160)
# Plot wavelength resolution:
a.figH()
# Plot transfer vector resolution:
a.figR()
# Example of data fitting with the resolution profile
# Create instance of data object from a texte file
d = pytof.data(file='SiO2_MR.txt')
# Plot reflectivity vs. index of tof-channel
d.plotraw()
# Fit data using with a model (here Fresnel's reflectivity) and plot
d.fit([1, 0.01, 0])
# Zoom to see details:
pytof.plt.xlim([60, 90])
pytof.plt.ylim([0.05, 1.1])
# etc.